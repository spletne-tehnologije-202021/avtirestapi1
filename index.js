const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const avtomobiliRoutes = require('./routes/avtomobili');
const cors = require('cors');

const PORT = process.env.PORT || 3000;
const MONGO_URL = process.env.MONGO_URL || 'mongodb+srv://<user>:<pass>@cluster0-mmhxp.mongodb.net/<database>?retryWrites=true&w=majority';

mongoose.connect(MONGO_URL,  {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).catch(err => console.log(err));;
mongoose.set('useFindAndModify', false);
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api/avtomobili', avtomobiliRoutes);
app.use(cors());
app.listen(PORT, () => {
    console.log(`Strežnik posluša na http://localhost:${PORT}`);
});