const Avtomobil = require('../models/avtomobil');
const express = require('express');
const router = express.Router();

// implementacija vmesnika
router.route('/').get(async(req, res) =>  {
    try {
        const avtomobili = await Avtomobil.find({});
        res.json(avtomobili);
    } catch (err) {
        console.log('Napaka: ' + err);
        res.status(500).send({ msg: err });
    }
});

router.route('/:id').get(async(req, res) =>  {
    await Avtomobil.findOne({ _id: req.params.id }, (err, avtomobil) => {
        if (err) {
            res.status(500).send({ msg: err });
        } else {
            res.json(avtomobil);
        }
    }).catch(err1 =>  {
        res.status(500).json({ msg: err1 });
    })

});

router.route('/').post(async(req, res) =>  {
    try {

        const novAvto = new Avtomobil(req.body);
        await novAvto.save(() =>  {
            let avtoUri = `${req.protocol}://${req.headers.host}${req.originalUrl}/${novAvto._id}`;
            res.setHeader('Location', avtoUri);
            res.send(novAvto);
        })

    } catch (error) {
        res.status(500).send(error);
    }
});

router.route('/:id').put(async(req, res) =>  {
    if (req.params.id !== req.body._id) {
        res.status(400).json('IDja nista enaka');
    } else {
        try {
            const avto = await Avtomobil.findByIdAndUpdate(req.params.id, req.body);
            res.send('Avto je bil posodobljen');
        } catch (error) {
            res.status(500).send(error);
        }
    }
});

router.route('/:id').delete(async(req, res) =>  {
    try {
        const brisi = await Avtomobil.findOne({ _id: req.params.id });
        if (!brisi) {
            res.status(404).json({ msg: `Avto z id=${req.params.id} ne obstaja` });
        } else {
            await Avtomobil.findByIdAndDelete(req.params.id);
            res.status(200).json({ msg: 'Avto je bil odstranjen' });
        }
    } catch (error) {
        res.status(500).send(error);
    }
});

module.exports = router;