const mongoose = require('mongoose');
const avtoSchema = new mongoose.Schema({
    znamka: {
        type: String,
        required: true,
        trim: true
    },
    model: {
        type: String,
        required: true,
        trim: true
    },
    letnik: {
        type: Number,
        required: true
    },
    cena: {
        type: Number,
        required: true
    }
});
const Avtomobil = mongoose.model('Avtomobil', avtoSchema);
module.exports = Avtomobil;